declare module 'svelte-inview/dist/index.mjs' {
	type InviewEvent = {
		detail: {
			inView: boolean;
		};
	};

	type InviewOptions = {
		root?: Element|null;
		rootMargin?: string;
		threshold?: number;
		unobserveOnEnter?: boolean;
	};

	const inview: (node?: Element, options?: InviewOptions) => {
		destroy: () => void;
	};

	export { inview, InviewEvent };
}
