import encounterConditionValueRepository from '~/services/repositories/encounter-condition-value.repository';
import encounterMethodRepository from '~/services/repositories/encounter-method.repository';
import type { Immutable } from '~/models/immutable.type';
import logger from '~/services/logger.service';
import persistentStorage from '~/services/persistent-storage.service';
import { storageBackedWritable } from '~/utilities/factories/storage-backed-store.factory';
import type { SvelteComponent } from 'svelte';
import { UIEncountersUncaughtOption } from '~/models/ui-encounters-uncaught-option.enum';
import { writable } from 'svelte/store';
import type {
	IEncounterConditionValue,
	IEncounterMethod
} from 'pokeapi-typescript';

const STORAGE_PREFIX = 'ui';
const ENCOUNTERS_PREFIX = `${STORAGE_PREFIX}-encounters`;
const FILTER_CONDITIONS_KEY = `${ENCOUNTERS_PREFIX}-filter-conditions`;
const FILTER_METHODS_KEY = `${ENCOUNTERS_PREFIX}-filter-methods`;
const SHOW_UNCAUGHT_KEY = `${ENCOUNTERS_PREFIX}-show-uncaught`;

const filterConditionsNotPresent = persistentStorage.get<string[]>(FILTER_CONDITIONS_KEY) === null;
const filterMethodsNotPresent = persistentStorage.get<string[]>(FILTER_METHODS_KEY) === null;

export const uiStore = {
	ui: {
		activeModal: writable<typeof SvelteComponent | null>(null),
		encounters: {
			filterConditions: storageBackedWritable<string[]>(FILTER_CONDITIONS_KEY, persistentStorage, []),
			filterMethods: storageBackedWritable<string[]>(FILTER_METHODS_KEY, persistentStorage, []),
			showUncaught: storageBackedWritable<UIEncountersUncaughtOption | false>(
				SHOW_UNCAUGHT_KEY,
				persistentStorage,
				UIEncountersUncaughtOption.THIS_VERSION
			)
		}
	},
	// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
	displayModal (component: typeof SvelteComponent): void {
		this.ui.activeModal.set(component);
	},
	hideModal (): void {
		this.ui.activeModal.set(null);
	}
};

const setDefaultFilterConditions = async (): Promise<void> => {
	const encounterConditions = await encounterConditionValueRepository.getAll();
	const encounterConditionNames = encounterConditions
		.map(({ name }: Immutable<IEncounterConditionValue>) => name)
		.filter((name, index, arr: readonly string[]) => arr.indexOf(name) === index);

	uiStore.ui.encounters.filterConditions.set(encounterConditionNames);
};

const setDefaultFilterMethods = async (): Promise<void> => {
	const encounterMethods = await encounterMethodRepository.getAll();
	const encounterMethodNames = encounterMethods
		.map(({ name }: Immutable<IEncounterMethod>) => name)
		.filter((name, index, arr: readonly string[]) => arr.indexOf(name) === index);

	uiStore.ui.encounters.filterMethods.set(encounterMethodNames);
};

if (filterConditionsNotPresent) setDefaultFilterConditions()
	// eslint-disable-next-line promise/prefer-await-to-callbacks
	.catch(err => { logger.error(err); });

if (filterMethodsNotPresent) setDefaultFilterMethods()
	// eslint-disable-next-line promise/prefer-await-to-callbacks
	.catch(err => { logger.error(err); });
