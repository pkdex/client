import type { ICaughtPokemon } from '~/models/caught-pokemon.model';
import languageRepository from '~/services/repositories/language.repository';
import persistentStorage from '~/services/persistent-storage.service';
import { storageBackedWritable } from '~/utilities/factories/storage-backed-store.factory';
import versionGroupRepository from '~/services/repositories/version-group.repository';
import versionRepository from '~/services/repositories/version.repository';
import {
	DEFAULT_LANGUAGE,
	DEFAULT_VERSION,
	DEFAULT_VERSION_GROUP
} from '~/app.config';
import {
	get,
	writable
} from 'svelte/store';
import type {
	ILanguage,
	ILocation,
	IVersion,
	IVersionGroup
} from 'pokeapi-typescript';

const STORAGE_PREFIX = 'user';
const LANGUAGE_STORAGE_KEY = `${STORAGE_PREFIX}-language`;
const POKEMON_STORAGE_KEY = `${STORAGE_PREFIX}-pokemon`;
const VERSION_STORAGE_KEY = `${STORAGE_PREFIX}-version`;
const VERSION_GROUP_STORAGE_KEY = `${STORAGE_PREFIX}-version-group`;

const userCurrentLocationStore = writable<ILocation | null>(null);
const userLanguageStore = storageBackedWritable<ILanguage>(LANGUAGE_STORAGE_KEY, persistentStorage, DEFAULT_LANGUAGE);
const userVersionStore = storageBackedWritable<IVersion>(VERSION_STORAGE_KEY, persistentStorage, DEFAULT_VERSION);
const userVersionGroupStore = storageBackedWritable<IVersionGroup>(
	VERSION_GROUP_STORAGE_KEY,
	persistentStorage,
	DEFAULT_VERSION_GROUP
);
const userPokemonStore = storageBackedWritable<ICaughtPokemon[]>(POKEMON_STORAGE_KEY, persistentStorage, []);

export const userStore = {
	user: {
		currentLocation: userCurrentLocationStore,
		language: userLanguageStore,
		pokemon: userPokemonStore,
		version: userVersionStore,
		versionGroup: userVersionGroupStore
	},
	catchPokemon (pokemonName: string, version: string): void {
		this.user.pokemon.update((pokemon: readonly Readonly<ICaughtPokemon>[]) => {
			const caughtPokemon: ICaughtPokemon = { version, pokemon: pokemonName, caught: Date.now() };
			const currentLocation = get(this.user.currentLocation);

			if (currentLocation) caughtPokemon.location = currentLocation.name;

			const newPokemon = [
				...pokemon,
				caughtPokemon
			];

			return newPokemon;
		});
	},
	releasePokemon (pokemonName: string, versionName: string): void {
		this.user.pokemon.update((caughtPokemon: readonly Readonly<ICaughtPokemon>[]) => {
			const newPokemon = [...caughtPokemon];

			const index = newPokemon.findIndex(({ pokemon, version }) => pokemon === pokemonName && version === versionName);

			if (index < 0) return newPokemon;

			newPokemon.splice(index, 1);

			return newPokemon;
		});
	},
	transferPokemon (pokemonName: string, fromVersion: string, toVersion: string): void {
		this.user.pokemon.update((caughtPokemon: readonly Readonly<ICaughtPokemon>[]) => {
			const newPokemon = [...caughtPokemon];

			const index = newPokemon.findIndex(({ pokemon, version }) => pokemon === pokemonName && version === fromVersion);

			if (!index) return newPokemon;

			newPokemon.splice(index, 1, {
				pokemon: pokemonName,
				version: toVersion,
				caught: newPokemon[index].caught
			});

			persistentStorage.set(POKEMON_STORAGE_KEY, newPokemon);

			return newPokemon;
		});
	},
	async setLanguage (id: number | string): Promise<ILanguage> {
		const language = await languageRepository.get(`${id}`);

		this.user.language.set(language);

		return language;
	},
	async setVersion (name: string): Promise<{ version: IVersion; versionGroup: IVersionGroup; }> {
		const version = await versionRepository.get(name);
		const versionGroup = await versionGroupRepository.get(version.version_group.name);

		this.user.version.set(version);
		this.user.versionGroup.set(versionGroup);

		return { version, versionGroup };
	}
};
