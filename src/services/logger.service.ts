import consola from 'consola';
import type { Immutable } from '~/models/immutable.type';

type Message = Immutable<unknown>;
type Messages = readonly Message[];

const logger = {
	error: (...messages: Messages): void => { consola.error(messages[0], ...messages.slice(1)); },
	info: (...messages: Messages): void => { consola.info(messages[0], ...messages.slice(1)); },
	log: (...messages: Messages): void => { consola.log(messages[0], ...messages.slice(1)); },
	success: (...messages: Messages): void => { consola.success(messages[0], ...messages.slice(1)); },
	warn: (...messages: Messages): void => { consola.warn(messages[0], ...messages.slice(1)); }
};

export default logger;
