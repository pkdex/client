import { getPokeApiListProducer } from '~/utilities/factories/list-producer.factory';
import type { Immutable } from '~/models/immutable.type';
import type { IVersionGroup } from 'pokeapi-typescript';
import PokeAPI from 'pokeapi-typescript';
import { Repository } from '~/models/repository.model';

const versionGroupIdentifier = ({ name }: Immutable<IVersionGroup>): string => name;

const listProducer = getPokeApiListProducer<IVersionGroup, string>(
	PokeAPI.VerionGroup.listAll.bind(PokeAPI.VerionGroup),
	({ name }) => name
);

export default new Repository(
	'version-groups',
	versionGroupIdentifier,
	PokeAPI.VerionGroup.resolve.bind(PokeAPI.VerionGroup),
	listProducer
);
