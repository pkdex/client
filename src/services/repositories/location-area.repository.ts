import type { Immutable } from '~/models/immutable.type';
import PokeAPI from 'pokeapi-typescript';
import { Repository } from '~/models/repository.model';
import type {
	ILocationArea,
	INamedApiResource
} from 'pokeapi-typescript';

const locationAreaIdentifier = ({ name }: Immutable<ILocationArea>): string => name;

const listProducer = async (): Promise<string[]> => {
	const { results } = await PokeAPI.LocationArea.listAll();

	return results
		.map(({ name }: Immutable<INamedApiResource<ILocationArea>>) => name);
};

export default new Repository(
	'location-areas',
	locationAreaIdentifier,
	PokeAPI.LocationArea.resolve.bind(PokeAPI.LocationArea),
	listProducer
);
