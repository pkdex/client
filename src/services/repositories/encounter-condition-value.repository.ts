import { getPokeApiListProducer } from '~/utilities/factories/list-producer.factory';
import type { IEncounterConditionValue } from 'pokeapi-typescript';
import type { Immutable } from '~/models/immutable.type';
import PokeAPI from 'pokeapi-typescript';
import { Repository } from '~/models/repository.model';

const encounterConditionValueIdentifier = ({ name }: Immutable<IEncounterConditionValue>): string => `${name}`;

const listProducer = getPokeApiListProducer<IEncounterConditionValue, string>(
	PokeAPI.EncounterConditionValue.listAll.bind(PokeAPI.EncounterConditionValue),
	({ name }) => name
);

export default new Repository(
	'encounter-condition-values',
	encounterConditionValueIdentifier,
	PokeAPI.EncounterConditionValue.resolve.bind(PokeAPI.EncounterConditionValue),
	listProducer
);
