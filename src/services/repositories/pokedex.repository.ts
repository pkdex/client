import { getPokeApiListProducer } from '~/utilities/factories/list-producer.factory';
import type { Immutable } from '~/models/immutable.type';
import type { IPokedex } from 'pokeapi-typescript';
import PokeAPI from 'pokeapi-typescript';
import { Repository } from '~/models/repository.model';

const pokedexIdentifier = ({ name }: Immutable<IPokedex>): string => name;

const listProducer = getPokeApiListProducer<IPokedex, string>(
	PokeAPI.Pokedex.listAll.bind(PokeAPI.Pokedex),
	({ name }) => name
);

export default new Repository(
	'pokedex',
	pokedexIdentifier,
	PokeAPI.Pokedex.resolve.bind(PokeAPI.Pokedex),
	listProducer
);
