import { getPokeApiListProducer } from '~/utilities/factories/list-producer.factory';
import type { Immutable } from '~/models/immutable.type';
import type { IVersion } from 'pokeapi-typescript';
import PokeAPI from 'pokeapi-typescript';
import { Repository } from '~/models/repository.model';

const versionIdentifier = ({ name }: Immutable<IVersion>): string => name;

const listProducer = getPokeApiListProducer<IVersion, string>(
	PokeAPI.Version.listAll.bind(PokeAPI.Version),
	({ name }) => name
);

export default new Repository(
	'versions',
	versionIdentifier,
	PokeAPI.Version.resolve.bind(PokeAPI.Version),
	listProducer
);
