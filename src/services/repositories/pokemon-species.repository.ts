import { getPokeApiListProducer } from '~/utilities/factories/list-producer.factory';
import type { Immutable } from '~/models/immutable.type';
import type { IPokemonSpecies } from 'pokeapi-typescript';
import PokeAPI from 'pokeapi-typescript';
import { Repository } from '~/models/repository.model';

const pokemonSpeciesIdentifier = ({ name }: Immutable<IPokemonSpecies>): string => name;

const listProducer = getPokeApiListProducer<IPokemonSpecies, string>(
	PokeAPI.PokemonSpecies.listAll.bind(PokeAPI.PokemonSpecies),
	({ name }) => name
);

export default new Repository(
	'pokemon-species',
	pokemonSpeciesIdentifier,
	PokeAPI.PokemonSpecies.resolve.bind(PokeAPI.PokemonSpecies),
	listProducer
);
