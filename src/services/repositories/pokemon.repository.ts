import { getPokeApiListProducer } from '~/utilities/factories/list-producer.factory';
import type { Immutable } from '~/models/immutable.type';
import type { IPokemon } from 'pokeapi-typescript';
import PokeAPI from 'pokeapi-typescript';
import { Repository } from '~/models/repository.model';

const pokemonIdentifier = ({ name }: Immutable<IPokemon>): string => name;

const listProducer = getPokeApiListProducer<IPokemon, string>(
	PokeAPI.Pokemon.listAll.bind(PokeAPI.Pokemon),
	({ name }) => name
);

export default new Repository(
	'pokemon',
	pokemonIdentifier,
	PokeAPI.Pokemon.resolve.bind(PokeAPI.Pokemon),
	listProducer
);
