import { getPokeApiListProducer } from '~/utilities/factories/list-producer.factory';
import type { Immutable } from '~/models/immutable.type';
import type { IRegion } from 'pokeapi-typescript';
import PokeAPI from 'pokeapi-typescript';
import { Repository } from '~/models/repository.model';

const regionIdentifier = ({ name }: Immutable<IRegion>): string => name;

const listProducer = getPokeApiListProducer<IRegion, string>(
	PokeAPI.Region.listAll.bind(PokeAPI.Region),
	({ name }) => name
);

export default new Repository(
	'regions',
	regionIdentifier,
	PokeAPI.Region.resolve.bind(PokeAPI.Region),
	listProducer
);
