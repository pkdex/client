import { getPokeApiListProducer } from '~/utilities/factories/list-producer.factory';
import type { IEncounterMethod } from 'pokeapi-typescript';
import type { Immutable } from '~/models/immutable.type';
import PokeAPI from 'pokeapi-typescript';
import { Repository } from '~/models/repository.model';

const encounterMethodIdentifier = ({ name }: Immutable<IEncounterMethod>): string => `${name}`;

const listProducer = getPokeApiListProducer<IEncounterMethod, string>(
	PokeAPI.EncounterMethod.listAll.bind(PokeAPI.EncounterMethod),
	({ name }) => name
);

export default new Repository(
	'encounter-methods',
	encounterMethodIdentifier,
	PokeAPI.EncounterMethod.resolve.bind(PokeAPI.EncounterMethod),
	listProducer
);
