import { getPokeApiListProducer } from '~/utilities/factories/list-producer.factory';
import type { ILocation } from 'pokeapi-typescript';
import type { Immutable } from '~/models/immutable.type';
import PokeAPI from 'pokeapi-typescript';
import { Repository } from '~/models/repository.model';

const locationIdentifier = ({ name }: Immutable<ILocation>): string => name;

const listProducer = getPokeApiListProducer<ILocation, string>(
	PokeAPI.Location.listAll.bind(PokeAPI.Location),
	({ name }) => name
);

export default new Repository(
	'locations',
	locationIdentifier,
	PokeAPI.Location.resolve.bind(PokeAPI.Location),
	listProducer
);
