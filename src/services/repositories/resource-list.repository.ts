import type { Immutable } from '~/models/immutable.type';
import PokeAPI from 'pokeapi-typescript';
import { Repository } from '~/models/repository.model';
import type { ResourceList } from '~/models/resource-list.model';
import { ResourceListName } from '~/models/resource-list.model';

type NamedEndpoint = typeof PokeAPI.Language
	| typeof PokeAPI.Version;

// eslint-disable-next-line consistent-return
const resourceListNameToGetterMapper = (name: ResourceListName): NamedEndpoint => {
	// eslint-disable-next-line default-case
	switch (name) {
		case ResourceListName.Language:
			return PokeAPI.Language;
		case ResourceListName.Version:
			return PokeAPI.Version;
	}
};

const resolver = async (name: ResourceListName): Promise<ResourceList> => {
	const endpoint = resourceListNameToGetterMapper(name);

	const getter = endpoint.listAll.bind(endpoint);

	const { results: resources } = await getter();

	return { name, resources };
};

const resourceListIdentifier = ({ name }: Immutable<ResourceList>): ResourceListName => name;

// eslint-disable-next-line @typescript-eslint/require-await
const listProducer = async (): Promise<ResourceListName[]> => Object.values(ResourceListName);

export default new Repository(
	'resource-lists',
	resourceListIdentifier,
	resolver,
	listProducer
);
