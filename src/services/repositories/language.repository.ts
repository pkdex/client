import { getIdFromUrl } from '~/utilities/pokeapi/get-id-from-url.utility';
import type { ILanguage } from 'pokeapi-typescript';
import type { Immutable } from '~/models/immutable.type';
import PokeAPI from 'pokeapi-typescript';
import { Repository } from '~/models/repository.model';
import { ResourceListName } from '~/models/resource-list.model';
import resourceListRepository from '~/services/repositories/resource-list.repository';
import type { ResourceStub } from '~/models/resource-list.model';

const languageIdentifier = ({ id }: Immutable<ILanguage>): string => `${id}`;

const listProducer = async (): Promise<string[]> => {
	const { resources } = await resourceListRepository.get(ResourceListName.Language);

	const ids = resources
		.map(({ url }: Readonly<ResourceStub>) => getIdFromUrl(url))
		.filter(id => id !== null) as number[];

	return ids.map(id => `${id}`);
};

export default new Repository<ILanguage, string>(
	'languages',
	languageIdentifier,
	PokeAPI.Language.resolve.bind(PokeAPI.Language),
	listProducer
);
