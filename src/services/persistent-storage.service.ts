import { Storage } from '~/models/storage.model';
import { STORAGE_PREFIX } from '~/app.config';

const persistentStorage = new Storage(STORAGE_PREFIX, localStorage);

export default persistentStorage;
