export enum ResourceListName {
	Language = 'language',
	Version = 'version'
}

export type ResourceStub = {
	name: string;
	url: string;
};

export type ResourceList = {
	name: ResourceListName;
	resources: ResourceStub[];
};
