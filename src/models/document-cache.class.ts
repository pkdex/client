import { byUnique } from '~/utilities/array/filter.utilities';
import { CachedDatabase } from '~/models/cached-database.model';
import { DEFAULT_CACHE_TIME_TO_LIVE } from '~/app.config';
import logger from '~/services/logger.service';

type CachedDocument <T> = {
	timestamp: number;
	contents: T;
};

export class DocumentCache <T, K extends T[keyof T] & string> {
	readonly #db: CachedDatabase<CachedDocument<T>, K>;
	readonly #identifier: (document: T) => K;
	readonly #timeToLive: number;
	readonly #valueProducer: (...args: readonly K[]) => Promise<T[]>;

	constructor (
		name: string,
		valueProducer: (...ids: readonly K[]) => Promise<T[]>,
		identifier: (document: T) => K,
		timeToLive: number = DEFAULT_CACHE_TIME_TO_LIVE
	) {
		this.#identifier = identifier.bind(this);
		this.#timeToLive = timeToLive;
		this.#valueProducer = valueProducer;
		// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
		this.#db = new CachedDatabase<CachedDocument<T>, K>(name, ({ contents }) => identifier(contents));
	}

	async resolve (...unfilteredIds: readonly K[]): Promise<T[]> {
		const ids = unfilteredIds.filter(byUnique(id => id));

		const currentTimestamp = Date.now();

		const cachedDocuments = await this.#db.getMany(...ids);

		const cachedDocumentIds = cachedDocuments
			// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
			.map(({ contents }) => this.#identifier(contents));

		const missingDocumentIds = ids.filter(id => !cachedDocumentIds.includes(id));

		const outdatedDocuments = cachedDocuments
			// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
			.filter(({ timestamp }) => currentTimestamp - timestamp >= this.#timeToLive);

		// eslint-disable-next-line @typescript-eslint/no-floating-promises
		if (outdatedDocuments.length > 0) this.#refreshDocuments(outdatedDocuments);

		const fetchedDocuments = await this.#valueProducer(...missingDocumentIds);

		// eslint-disable-next-line @typescript-eslint/no-floating-promises
		this.#insertDocuments(fetchedDocuments);

		return [
			...fetchedDocuments,
			// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
			...cachedDocuments.map(({ contents }) => contents)
		];
	}

	async #insertDocuments (docs: readonly T[]): Promise<void> {
		try {
			await Promise.all(docs.map(async contents => this.#db.put({ timestamp: Date.now(), contents })));
		} catch (err) {
			logger.error(err);
		}
	}

	// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
	async #refreshDocuments (docs: CachedDocument<T>[]): Promise<void> {
		// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
		const docIds = docs.map(({ contents }) => this.#identifier(contents));

		const fetchedDocuments = await this.#valueProducer(...docIds);

		const updatedDocuments = fetchedDocuments.map(contents => ({ contents, timestamp: Date.now() }));

		try {
			await Promise.all(updatedDocuments.map(async (doc: Readonly<CachedDocument<T>>) => this.#db.put(doc)));
		} catch (err) {
			logger.error(err);
		}
	}
}
