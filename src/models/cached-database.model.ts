/* eslint-disable no-underscore-dangle */
import { Database } from '~/models/database.model';
import { DEFAULT_CACHE_SIZE } from '~/app.config';
import { FifoCache } from '~/models/fifo-cache.model';
import logger from '~/services/logger.service';

export class CachedDatabase <T extends object, K extends string> extends Database<T, K> {
	readonly #cache: FifoCache<T, K>;

	constructor (
		name: string,
		identifier: (record: T) => K,
		cacheSize = DEFAULT_CACHE_SIZE
	) {
		super(name, identifier);
		this.#cache = new FifoCache<T, K>(identifier, cacheSize);
		// eslint-disable-next-line @typescript-eslint/no-floating-promises
		this._refreshStoredIds();
	}

	async get (id: K): Promise<T | null> {
		if (this.#cache.has(id)) return this.#cache.get(id);

		try {
			const record = await this._db.get(id);

			this.#cache.add(record);

			return record;
		} catch (err) {
			logger.error(err);

			return null;
		}
	}

	async getMany (...ids: readonly K[]): Promise<T[]> {
		const [cachedIds, uncachedIds] = ids
			.reduce(([cached, uncached]: readonly (readonly K[])[], id) => {
				const isCached = this.#cache.has(id);

				if (isCached) return [[...cached, id], uncached];

				return [cached, [...uncached, id]];
			}, [[], []]);

		const cachedRecords = cachedIds.map(id => this.#cache.get(id)) as T[];

		const uncachedRecords: T[] = [];

		if (uncachedIds.length > 0) {
			// eslint-disable-next-line @typescript-eslint/naming-convention
			const { rows: allRecords } = await this._db.allDocs({ include_docs: true });

			const matchingRecords = allRecords
				// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
				.filter(({ doc }) => doc && uncachedIds.includes(this._identifier(doc)))
				// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
				.map(({ doc }) => doc as T);

			uncachedRecords.push(...matchingRecords);
		}

		const records = [...cachedRecords, ...uncachedRecords]
			.sort((recordA, recordB) => {
				const idA = this._identifier(recordA);
				const idB = this._identifier(recordB);

				const indexA = ids.indexOf(idA);
				const indexB = ids.indexOf(idB);

				if (indexA < indexB) return 1;
				if (indexA > indexB) return -1;

				return 0;
			});

		records.forEach(record => { this.#cache.add(record); });

		return records;
	}
}
