import { DEFAULT_CACHE_SIZE } from '~/app.config';

export type IFifoCache <T> = {
	readonly cache: T[];
	add: (item: T) => void;
};

export class FifoCache <T, K extends string> implements IFifoCache<T> {
	readonly #identifier: (record: T) => K;
	readonly #size: number;
	#cache: T[] = [];

	constructor (identifier: (record: T) => K, size = DEFAULT_CACHE_SIZE) {
		this.#size = size;
		this.#identifier = identifier.bind(this);
	}

	get cache (): T[] {
		return [...this.#cache];
	}

	get #cachedIds (): K[] {
		return this.#cache.map(item => this.#identifier(item));
	}

	add (item: T): void {
		const id = this.#identifier(item);

		const itemIndex = this.#cachedIds.indexOf(id);

		if (itemIndex >= 0) this.#cache.splice(itemIndex, 1);
		else if (this.#cache.length === this.#size) this.#cache.shift();

		this.#cache.push(item);
	}

	has (id: K): boolean {
		return this.#cachedIds.includes(id);
	}

	get (id: K): T | null {
		if (!this.has(id)) return null;

		const index = this.#cachedIds.indexOf(id);

		return this.#cache[index];
	}
}
