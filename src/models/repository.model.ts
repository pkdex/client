import { DocumentCache } from '~/models/document-cache.class';

export type IRepository <T, K extends T[keyof T]> = {
	getAll: (...ids: readonly K[]) => Promise<T[]>;
	get: (id: K) => Promise<T>;
};

export class Repository <T, K extends T[keyof T] & string> implements IRepository<T, K> {
	readonly #name: string;
	readonly #cache: DocumentCache<T, K>;
	#listProducer: () => Promise<K[]>;

	constructor (
		name: string,
		identifier: (value: T) => K,
		resolver: (id: K) => Promise<T>,
		listProducer: () => Promise<K[]>
	) {
		this.#name = name;

		const getter = async (...ids: readonly K[]): Promise<T[]> => {
			const promises = ids.map(async id => resolver(id));

			const settledPromises = await Promise.allSettled(promises);

			const values = settledPromises
				.filter(({ status }: Readonly<PromiseSettledResult<Awaited<T>>>) => status === 'fulfilled')
				.map((result: Readonly<PromiseSettledResult<Awaited<T>>>) => (result as PromiseFulfilledResult<T>).value);

			return values;
		};

		this.#cache = new DocumentCache<T, K>(
			this.#name,
			getter,
			identifier
		);

		this.#listProducer = listProducer.bind(this);
	}

	async getAll (...ids: readonly K[]): Promise<T[]> {
		if (ids.length > 0) return this.#cache.resolve(...ids);

		const allIds = await this.#listProducer();

		return this.#cache.resolve(...allIds);
	}

	async get (id: K): Promise<T> {
		const values = await this.getAll(id);

		if (values.length <= 0) throw new Error(`${this.#name} Repository - Could not resolve request for id: ${JSON.stringify(id)}`);

		const [value] = values;

		return value;
	}
}
