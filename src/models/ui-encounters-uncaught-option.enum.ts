export enum UIEncountersUncaughtOption {
	THIS_VERSION = 'this-version',
	ALL_VERSIONS = 'all-versions'
}
