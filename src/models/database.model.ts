/* eslint-disable no-underscore-dangle */
import logger from '~/services/logger.service';
import PouchDB from 'pouchdb';

export type IDatabase <T, K extends string> = {
	get: (id: K) => Promise<T | null>;
	getMany: (...ids: readonly K[]) => Promise<T[]>;
	put: (record: T) => Promise<T | null>;
};

export class Database <T extends object, K extends string> implements IDatabase<T, K> {
	protected readonly _name: string;
	protected readonly _db: PouchDB.Database<T>;
	protected readonly _identifier: (record: T) => K;
	protected _storedIds: K[] = [];

	constructor (
		name: string,
		identifier: (record: T) => K
	) {
		this._name = name;
		this._db = new PouchDB<T>(this._name);
		this._identifier = identifier.bind(this);
	}

	async get (id: K): Promise<T | null> {
		try {
			const record = await this._db.get(id);

			return record;
		} catch (err) {
			logger.error(err);

			return null;
		}
	}

	async getMany (...ids: readonly K[]): Promise<T[]> {
		// eslint-disable-next-line @typescript-eslint/naming-convention
		const { rows: allRecords } = await this._db.allDocs({ include_docs: true });

		const records = allRecords
			// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
			.filter(({ doc }) => doc && ids.includes(this._identifier(doc)))
			// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
			.map(({ doc }) => doc as T);

		return records;
	}

	async put (record: T): Promise<T | null> {
		const id = this._identifier(record);

		let result: T | null = null;

		if (this._storedIds.includes(id)) result = await this._update(id, record);
		else result = await this._insert(id, record);

		// eslint-disable-next-line @typescript-eslint/no-floating-promises
		this._refreshStoredIds();

		return result;
	}

	protected async _refreshStoredIds (): Promise<void> {
		const { rows } = await this._db.allDocs();

		this._storedIds = rows.map(({ id }: Readonly<{ id: string; }>) => id as K);
	}

	protected async _insert (id: K, record: T): Promise<T | null> {
		try {
			await this._db.put({ _id: id, ...record });

			return this.get(id);
		} catch (err) {
			logger.error(`Error inserting document identified as ${id}: `, err);

			return null;
		}
	}

	protected async _update (id: K, record: T): Promise<T | null> {
		try {
			const existing = await this._db.get(id);

			await this._db.put({ ...existing, ...record });

			return this.get(id);
		} catch (err) {
			logger.error(`Error updating document identified as ${id}: `, err);

			return null;
		}
	}
}
