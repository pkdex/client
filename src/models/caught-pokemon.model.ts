export type ICaughtPokemon = {
	pokemon: string;
	version: string;
	caught: number;
	location?: string;
};
