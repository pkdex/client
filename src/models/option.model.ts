export type IOption = {
	label: string;
	value: boolean | number | string;
};
