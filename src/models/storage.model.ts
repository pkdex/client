import type { IStorageProvider } from '~/models/storage-provider.model';
import logger from '~/services/logger.service';

export type IStorage = {
	clear: () => unknown;
	set: (key: string, value: unknown) => unknown;
	get: <T> (key: string) => T | null;
	remove: (key: string) => unknown;
};

export class Storage implements IStorage {
	readonly #name: string;
	readonly #storage: Readonly<IStorageProvider>;

	constructor (
		name: string,
		storage: Readonly<IStorageProvider>
	) {
		this.#name = name;
		this.#storage = storage;
	}

	clear (): void {
		this.#storage.clear();
	}

	set (key: string, value: unknown): void {
		const fullKey = this.#generateKey(key);

		this.#storage.setItem(fullKey, JSON.stringify(value));
	}

	get <T> (key: string): T | null {
		const fullKey = this.#generateKey(key);

		const storedValue = this.#storage.getItem(fullKey);

		if (storedValue === null) return null;

		try {
			const parsedValue = JSON.parse(storedValue) as T;

			return parsedValue;
		} catch (err) {
			this.remove(fullKey);
			logger.error(err);

			return null;
		}
	}

	remove (key: string): void {
		const fullKey = this.#generateKey(key);

		this.#storage.removeItem(fullKey);
	}

	#generateKey (key: string): string {
		return [this.#name, key].join('-');
	}
}
