export type InviewEvent = Event & {
	detail: {
		inView: boolean;
	};
};
