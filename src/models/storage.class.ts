import logger from '~/services/logger.service';
import { STORAGE_PREFIX } from '~/app.config';

type StorageBackend = {
	clear: () => void;
	getItem: (key: string) => string | null;
	setItem: (key: string, value: string) => void;
	removeItem: (key: string) => void;
};

export class Storage {
	#storage: StorageBackend;

	// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
	constructor (storage: StorageBackend) {
		this.#storage = storage;
	}

	clear (): void {
		this.#storage.clear();
	}

	set (key: string, value: unknown): void {
		this.#storage.setItem(`${STORAGE_PREFIX}${key}`, JSON.stringify(value));
	}

	get <T> (key: string): T | null {
		const storedValue = this.#storage.getItem(`${STORAGE_PREFIX}${key}`);

		if (storedValue === null) return null;

		try {
			const parsedValue = JSON.parse(storedValue) as T;

			return parsedValue;
		} catch (err) {
			logger.error(err);

			return null;
		}
	}

	remove (key: string): void {
		this.#storage.removeItem(`${STORAGE_PREFIX}${key}`);
	}
}
