export type User = {
	language: string | null;
	version: string | null;
	versionGroup: string | null;
};
