export type EncounterDetail = {
	chance: number;
	// eslint-disable-next-line @typescript-eslint/naming-convention
	condition_values: {
		name: string;
		url: string;
	}[];
	method: {
		name: string;
	};
};
