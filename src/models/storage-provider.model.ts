export type IStorageProvider = {
	getItem: (key: string) => string | null;
	setItem: (key: string, value: string) => void;
	clear: () => void;
	removeItem: (key: string) => void;
};
