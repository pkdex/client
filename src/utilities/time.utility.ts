const MS_IN_SECOND = 1000;
const SECONDS_IN_MINUTE = 60;
const MINUTES_IN_HOUR = 60;
const HOURS_IN_DAY = 24;
const DAYS_IN_WEEK = 7;

type Time = {
	readonly milliseconds: number;
	readonly seconds: number;
	readonly minutes: number;
	readonly hours: number;
	readonly days: number;
	readonly weeks: number;
};

export const time = (val: number): Time => ({
	get milliseconds (): number { return val; },
	get seconds (): number { return val * MS_IN_SECOND; },
	get minutes (): number { return this.seconds * SECONDS_IN_MINUTE; },
	get hours (): number { return this.minutes * MINUTES_IN_HOUR; },
	get days (): number { return this.hours * HOURS_IN_DAY; },
	get weeks (): number { return this.days * DAYS_IN_WEEK; }
});
