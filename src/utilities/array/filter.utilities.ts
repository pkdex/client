export const byUnique = <T = unknown, K = unknown> (
	identifier: (item: T) => K
) => (
	item: T,
	index: number,
	arr: readonly T[]
): boolean => {
	const itemId = identifier(item);

	const otherIndex = arr
		.findIndex(otherItem => identifier(otherItem) === itemId);

	return otherIndex === index;
};
