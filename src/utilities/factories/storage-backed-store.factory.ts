import type { IStorage } from '~/models/storage.model';
import persistentStorage from '~/services/persistent-storage.service';
import {
	readable,
	writable
} from 'svelte/store';

import type {
	Readable,
	Writable
} from 'svelte/store';

const storageBackedStore = <T> (
	name: string,
	storage: Readonly<IStorage>,
	storeFactory: typeof readable | typeof writable,
	defaultValue?: Readonly<T>
): Readable<T> | Writable<T> => {
	const storedValue = storage.get<T>(name);

	// eslint-disable-next-line @typescript-eslint/init-declarations
	let store;

	if (storedValue !== null) store = storeFactory<T>(storedValue);
	else if (defaultValue) store = storeFactory<T>(defaultValue);
	else store = storeFactory<T>();

	store.subscribe(value => {
		// eslint-disable-next-line no-undefined
		if (value === null || value === undefined) storage.remove(name);
		else storage.set(name, value);
	});

	return store;
};

export const storageBackedWritable = <T> (
	name: string,
	storage: Readonly<IStorage> = persistentStorage,
	defaultValue?: Readonly<T>
): Writable<T> => storageBackedStore<T>(name, storage, writable, defaultValue) as Writable<T>;

export const storageBackedReadable = <T> (
	name: string,
	storage: Readonly<IStorage> = persistentStorage,
	defaultValue?: Readonly<T>
): Readable<T> => storageBackedStore<T>(name, storage, readable, defaultValue) as Readable<T>;
