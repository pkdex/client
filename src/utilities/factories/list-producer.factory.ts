import type {
	INamedApiResource,
	INamedApiResourceList
} from 'pokeapi-typescript';

export const getPokeApiListProducer = <T, K> (
	lister: () => Promise<INamedApiResourceList<T>>,
	mapper: (listItem: Readonly<INamedApiResource<T>>) => K
) => async (): Promise<K[]> => {
	const { results } = await lister();

	return results.map((listItem: Readonly<INamedApiResource<T>>) => mapper(listItem));
};
