type StyleParams = {
	[key: string]: string;
};

type StyleUpdater = {
	update: (newStyles: Readonly<StyleParams>) => void;
};

// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
export const styles = (node: HTMLElement, params: Readonly<StyleParams>): StyleUpdater => {
	const setCustomProperties = (styleParams: Readonly<StyleParams>): void => {
		Object.entries(styleParams).forEach(([key, value]: readonly [string, string]) => {
			node.style.setProperty(`--${key}`, value);
		});
	};

	setCustomProperties(params);

	return {
		update (newStyles: Readonly<StyleParams>): void {
			setCustomProperties(newStyles);
		}
	};
};
