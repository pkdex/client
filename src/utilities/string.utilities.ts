export const dashCase = (str: string): string => str
	.toLowerCase()
	.replace(/\s+/gu, '-')
	.replace(/[^a-z\-0-9]/gu, '');

export const dashToCaps = (str: string): string => str
	.split('-')
	.map(part => `${part.charAt(0).toUpperCase()}${part.substring(1)}`)
	.join(' ');
