export const getIdFromUrl = (url: string): number | null => {
	const indexOfId = 4;

	const id = Number(new URL(url).pathname.split('/')[indexOfId]);

	if (isNaN(id)) return null;

	return id;
};
