import { dashToCaps } from '~/utilities/string.utilities';

const subtitleRgx = /--(?<subtitle>.+)$/u;

export const getNameSubtitle = (name: string): string | null => {
	const match = subtitleRgx.exec(name);

	if (match === null) return null;

	const { groups } = match;

	if (!groups) return null;

	const { subtitle } = groups;

	if (subtitle.length <= 0) return null;

	return dashToCaps(subtitle);
};
