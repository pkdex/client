import type { ILocationArea } from 'pokeapi-typescript';
import type { Immutable } from '~/models/immutable.type';

export const hasEncounterInVersion = (
	{ pokemon_encounters: encounters }: Immutable<ILocationArea>,
	versionName: string
): boolean => encounters
	.some(({ version_details: versionDetails }) => versionDetails
		.some(({ version: { name } }) => name === versionName));
