import { DEFAULT_LANGUAGE } from '~/app.config';
import type { Immutable } from '~/models/immutable.type';
import type {
	ILanguage,
	IName
} from 'pokeapi-typescript';

export const getLocalizedName = (
	names: readonly Immutable<IName>[],
	language: Immutable<ILanguage> | null = DEFAULT_LANGUAGE
): string => {
	if (names.length <= 0) return '';

	let matchingNames: IName[] = names.filter(({ language: { name: languageName } }) => languageName === language?.name);

	if (matchingNames.length > 0) return matchingNames[0].name;

	matchingNames = names.filter(({ language: { name: languageName } }) => languageName === DEFAULT_LANGUAGE.name);

	if (matchingNames.length > 0) return matchingNames[0].name;

	return names[0].name;
};
