// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
export const targetWithinElement = (target: EventTarget, element: Element): boolean => {
	let node: Element = target as Element;

	while (node !== element && node.parentNode) node = node.parentNode as Element;

	return node === element;
};
