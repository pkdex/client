import { time } from '~/utilities/time.utility';
import type {
	ILanguage,
	IVersion,
	IVersionGroup
} from 'pokeapi-typescript';

// Animation Settings
// eslint-disable-next-line @typescript-eslint/no-magic-numbers
export const { seconds: DEFAULT_ANIMATION_DURATION } = time(0.25);

// Cache/Storage Settings
export const DEFAULT_CACHE_SIZE = 100;
export const { days: DEFAULT_CACHE_TIME_TO_LIVE } = time(1);
export const STORAGE_PREFIX = 'pkdex';

// PokeAPI Vars
export const NATIONAL_DEX_NAME = 'national';

// User Defaults
export const DEFAULT_LANGUAGE: ILanguage = {
	id: 9,
	iso3166: 'us',
	iso639: 'en',
	name: 'en',
	names: [
		{
			language: {
				name: 'ja-Hrkt',
				url: 'https://pokeapi.co/api/v2/language/1/'
			},
			name: '英語'
		},
		{
			language: {
				name: 'ko',
				url: 'https://pokeapi.co/api/v2/language/3/'
			},
			name: '영어'
		},
		{
			language: {
				name: 'fr',
				url: 'https://pokeapi.co/api/v2/language/5/'
			},
			name: 'Anglais'
		},
		{
			language: {
				name: 'de',
				url: 'https://pokeapi.co/api/v2/language/6/'
			},
			name: 'Englisch'
		},
		{
			language: {
				name: 'es',
				url: 'https://pokeapi.co/api/v2/language/7/'
			},
			name: 'Inglés'
		},
		{
			language: {
				name: 'en',
				url: 'https://pokeapi.co/api/v2/language/9/'
			},
			name: 'English'
		}
	],
	official: true
};

export const DEFAULT_VERSION: IVersion = {
	id: 1,
	name: 'red',
	names: [
		{
			language: {
				name: 'ja-Hrkt',
				url: 'https://pokeapi.co/api/v2/language/1/'
			},
			name: '赤'
		},
		{
			language: {
				name: 'ko',
				url: 'https://pokeapi.co/api/v2/language/3/'
			},
			name: '레드'
		},
		{
			language: {
				name: 'fr',
				url: 'https://pokeapi.co/api/v2/language/5/'
			},
			name: 'Rouge'
		},
		{
			language: {
				name: 'de',
				url: 'https://pokeapi.co/api/v2/language/6/'
			},
			name: 'Rot'
		},
		{
			language: {
				name: 'es',
				url: 'https://pokeapi.co/api/v2/language/7/'
			},
			name: 'Rojo'
		},
		{
			language: {
				name: 'it',
				url: 'https://pokeapi.co/api/v2/language/8/'
			},
			name: 'Rossa'
		},
		{
			language: {
				name: 'en',
				url: 'https://pokeapi.co/api/v2/language/9/'
			},
			name: 'Red'
		}
	],
	// eslint-disable-next-line @typescript-eslint/naming-convention
	version_group: {
		name: 'red-blue',
		url: 'https://pokeapi.co/api/v2/version-group/1/'
	}
};

export const DEFAULT_VERSION_GROUP: IVersionGroup = {
	generation: {
		name: 'generation-i',
		url: 'https://pokeapi.co/api/v2/generation/1/'
	},
	id: 1,
	// eslint-disable-next-line @typescript-eslint/naming-convention
	move_learn_methods: [
		{
			name: 'level-up',
			url: 'https://pokeapi.co/api/v2/move-learn-method/1/'
		},
		{
			name: 'machine',
			url: 'https://pokeapi.co/api/v2/move-learn-method/4/'
		},
		{
			name: 'stadium-surfing-pikachu',
			url: 'https://pokeapi.co/api/v2/move-learn-method/5/'
		}
	],
	name: 'red-blue',
	order: 1,
	pokedexes: [
		{
			name: 'kanto',
			url: 'https://pokeapi.co/api/v2/pokedex/2/'
		}
	],
	regions: [
		{
			name: 'kanto',
			url: 'https://pokeapi.co/api/v2/region/1/'
		}
	],
	versions: [
		{
			name: 'red',
			url: 'https://pokeapi.co/api/v2/version/1/'
		},
		{
			name: 'blue',
			url: 'https://pokeapi.co/api/v2/version/2/'
		}
	]
};
